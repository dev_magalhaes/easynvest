docker stack rm $(docker stack ls  | grep 'custody' | awk '{print $1}') || true
docker config rm conf_custody || true
docker stack deploy --compose-file docker-compose.yaml custody