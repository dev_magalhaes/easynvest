using System.Threading.Tasks;
using Easynvest.Integration.Models;

namespace Easynvest.Integration.IServices
{
    public interface IRestClientService
    {
        Task<RestResponseModel<T>> GetAsync<T>(string requestUri, string mediaType) where T : class;
    }
}