using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Easynvest.Integration.Models;
using Easynvest.Integration.IServices;

namespace Easynvest.Integration.Services
{
    public class RestClientService : IRestClientService
    {
        private readonly ILogger<RestClientService> logger;
        private readonly HttpClient httpClient;

        public RestClientService(ILogger<RestClientService> logger, HttpClient httpClient)
        {
            this.logger = logger;
            this.httpClient = httpClient;
        }

        public async Task<RestResponseModel<T>> GetAsync<T>(string requestUri, string mediaType) where T : class
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, requestUri);
                MediaTypeHeaderValue mediaTypeHeader = new MediaTypeHeaderValue(mediaType);
                using (HttpResponseMessage httpResponse = await httpClient.SendAsync(request))
                {
                    if (!string.Equals(mediaType, httpResponse.Content.Headers.ContentType.MediaType, StringComparison.InvariantCultureIgnoreCase))
                        return Error(statusCode: httpResponse.StatusCode);

                    string response = await httpResponse.Content.ReadAsStringAsync();
                    if (string.IsNullOrWhiteSpace(response))
                        return Error(statusCode: httpResponse.StatusCode);

                    return new RestResponseModel<T>
                    {
                        Data = JsonConvert.DeserializeObject<T>(response)
                    };
                }
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"{nameof(GetAsync)} - {ex.Message}");
                return Error();
            }

            static RestResponseModel<T> Error(string response = null, HttpStatusCode? statusCode = null) => new RestResponseModel<T>
            {
                Error = true,
                ResponseText = response,
                StatusCode = statusCode == null ? HttpStatusCode.InternalServerError : statusCode.Value
            };
        }
    }
}