using System;
using System.Runtime.Serialization;

namespace Easynvest.Integration.Models
{
    [DataContract]
    public class TDModel
    {

        [DataMember(Name = "valorInvestido")]
        public double ValorInvestido { get; set; }

        [DataMember(Name = "valorTotal")]
        public double ValorTotal { get; set; }

        [DataMember(Name = "vencimento")]
        public DateTime Vencimento { get; set; }

        [DataMember(Name = "dataDeCompra")]
        public DateTime DataDeCompra { get; set; }

        [DataMember(Name = "iof")]
        public int Iof { get; set; }

        [DataMember(Name = "indice")]
        public string Indice { get; set; }

        [DataMember(Name = "tipo")]
        public string Tipo { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }
    }
}