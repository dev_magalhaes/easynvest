using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Easynvest.Integration.Models
{
    [DataContract]
    public class FixedIncomeResponseModel
    {
        [DataMember(Name = "lcis")]
        public IList<FixedIncomeModel> Lcis { get; set; }
    }
}