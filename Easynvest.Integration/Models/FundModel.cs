using System;
using System.Runtime.Serialization;

namespace Easynvest.Integration.Models
{
    [DataContract]
    public class FundModel
    {
        [DataMember(Name = "capitalInvestido")]
        public double CapitalInvestido { get; set; }

        [DataMember(Name = "ValorAtual")]
        public double ValorAtual { get; set; }

        [DataMember(Name = "dataResgate")]
        public DateTime DataResgate { get; set; }

        [DataMember(Name = "dataCompra")]
        public DateTime DataCompra { get; set; }

        [DataMember(Name = "iof")]
        public int Iof { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "totalTaxas")]
        public double TotalTaxas { get; set; }

        [DataMember(Name = "quantity")]
        public int Quantity { get; set; }
    }
}