using System;
using System.Runtime.Serialization;

namespace Easynvest.Integration.Models
{
    [DataContract]
    public class FixedIncomeModel
    {
        [DataMember(Name = "capitalInvestido")]
        public double CapitalInvestido { get; set; }

        [DataMember(Name = "capitalAtual")]
        public double CapitalAtual { get; set; }

        [DataMember(Name = "quantidade")]
        public double Quantidade { get; set; }

        [DataMember(Name = "vencimento")]
        public DateTime Vencimento { get; set; }

        [DataMember(Name = "iof")]
        public double Iof { get; set; }

        [DataMember(Name = "outrasTaxas")]
        public double OutrasTaxas { get; set; }

        [DataMember(Name = "taxas")]
        public double Taxas { get; set; }

        [DataMember(Name = "indice")]
        public string Indice { get; set; }

        [DataMember(Name = "tipo")]
        public string Tipo { get; set; }

        [DataMember(Name = "nome")]
        public string Nome { get; set; }

        [DataMember(Name = "guarantidoFGC")]
        public bool GuarantidoFGC { get; set; }

        [DataMember(Name = "dataOperacao")]
        public DateTime DataOperacao { get; set; }

        [DataMember(Name = "precoUnitario")]
        public double PrecoUnitario { get; set; }

        [DataMember(Name = "primario")]
        public bool Primario { get; set; }
    }
}