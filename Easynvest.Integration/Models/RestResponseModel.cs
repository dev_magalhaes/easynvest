using System.ComponentModel;
using System.Net;

namespace Easynvest.Integration.Models
{
    public class RestResponseModel<T> where T : class
    {
        public T Data { get; set; }

        public bool Error { get; set; }

        public string ResponseText { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}