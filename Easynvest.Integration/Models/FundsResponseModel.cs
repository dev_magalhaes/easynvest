using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Easynvest.Integration.Models
{
    [DataContract]
    public class FundsResponseModel
    {
        [DataMember(Name = "fundos")]
        public IList<FundModel> Fundos { get; set; }
    }
}