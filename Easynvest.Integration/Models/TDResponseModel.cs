using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Easynvest.Integration.Models
{
    [DataContract]
    public class TDResponseModel
    {

        [DataMember(Name = "tds")]
        public IList<TDModel> Tds { get; set; }
    }
}