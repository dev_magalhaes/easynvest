using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;

namespace Easyvest.Test
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureKestrel(serverOptions =>
                    {
                        serverOptions.AddServerHeader = false;
                        serverOptions.DisableStringReuse = false;
                    })
                    .UseStartup<TStartup>()
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseUrls($"http://*:80/");

            builder.ConfigureServices(services =>
            {

            });            
        }
    }
}