using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using Easynvest.Api;
using Easynvest.Domain.Models;

namespace Easyvest.Tests.Tests.Api
{
    public class CustodyTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;
        public CustodyTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Theory]
        [InlineData("/investments")]
        public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
        {
            var client = _factory.CreateClient();
            var response = await client.GetAsync(url);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
        }

        [Fact]
        public async Task Get_EndpointsReturnSuccessAndArrayBody()
        {
            var client = _factory.CreateClient();
            var response = await client.GetAsync("/investments");

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
            string body = await response.Content.ReadAsStringAsync();
            Assert.True(!string.IsNullOrEmpty(body));
            ClientCustody clientCustody = JsonConvert.DeserializeObject<ClientCustody>(body);
            Assert.NotNull(clientCustody);
            Assert.NotNull(clientCustody.Investiments);
        }
    }
}