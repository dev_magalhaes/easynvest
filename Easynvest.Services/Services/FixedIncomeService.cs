using System;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Easynvest.Domain.Models;
using Easynvest.Domain.Configs;
using Easynvest.Services.IServices;
using Easynvest.Integration.Models;
using Easynvest.Integration.IServices;
using System.Globalization;
using Newtonsoft.Json;

namespace Easynvest.Services.Services
{
    public class FixedIncomeService : IFixedIncomeService
    {
        private readonly Parameters parameters;
        private readonly ILogger<IFixedIncomeService> logger;
        private readonly IRestClientService restClientService;

        public FixedIncomeService(ILogger<IFixedIncomeService> logger, IRestClientService restClientService, IOptionsSnapshot<Parameters> parameters)
        {
            this.logger = logger;
            this.parameters = parameters.Value;
            this.restClientService = restClientService;
        }

        public async Task<IList<Investiment>> GetInvestimentsAsync()
        {
            RestResponseModel<FixedIncomeResponseModel> fixedIncomeResponse = await restClientService.GetAsync<FixedIncomeResponseModel>(parameters.Endpoints.FixedIncomeApi, MediaTypeNames.Application.Json);
            if (fixedIncomeResponse.Error)
            {
                if (!string.IsNullOrEmpty(fixedIncomeResponse.ResponseText))
                    logger.LogError($"{nameof(GetInvestimentsAsync)} - {fixedIncomeResponse.ResponseText}");
                return new List<Investiment>();
            }

            if (fixedIncomeResponse.Data != null && fixedIncomeResponse.Data.Lcis != null && fixedIncomeResponse.Data.Lcis.Any())
            {
                IList<Investiment> investiments = fixedIncomeResponse.Data.Lcis
                        .Select(lci => new Investiment
                        {
                            Expiration = lci.Vencimento,
                            TotalValue = lci.CapitalAtual,
                            InvestedValue = lci.CapitalInvestido,
                            Profitability = CalculateProfitability(lci),
                            Name = lci.Nome,
                            RedemptionValue = CalculateRedemptionValue(lci),
                        })
                        .ToList();

                logger.LogDebug($"investiments: {JsonConvert.SerializeObject(investiments)}");
                return investiments;
            }
            logger.LogInformation(parameters.Messages.EmptyInvestiment);
            return new List<Investiment>();
        }

        private double CalculateProfitability(FixedIncomeModel lci)
        {
            double profitability = (lci.CapitalAtual - lci.CapitalInvestido) * Convert.ToDouble(parameters.Profitability.FixedIncomeRate, CultureInfo.InvariantCulture);
            return Math.Round(profitability, int.Parse(parameters.Rounding));
        }

        private double CalculateRedemptionValue(FixedIncomeModel lci)
        {
            TimeSpan totalTime = lci.Vencimento.Subtract(lci.DataOperacao);
            TimeSpan runningTime = DateTime.Now.Subtract(lci.DataOperacao);

            double percentageDifference = runningTime / totalTime;
            double redemptionValue = 0;

            if (percentageDifference > 0.5)
            {
                redemptionValue = lci.CapitalAtual * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyTimeRate, CultureInfo.InvariantCulture));
            }
            else if (lci.DataOperacao.AddMonths(int.Parse(parameters.Redemption.CustodyTimeMonth)) >= lci.Vencimento)
            {
                redemptionValue = lci.CapitalAtual * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyTimeMonthRate, CultureInfo.InvariantCulture));
            }
            else
            {
                redemptionValue = lci.CapitalAtual * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyDefaultRate, CultureInfo.InvariantCulture));
            }

            return Math.Round(redemptionValue, int.Parse(parameters.Rounding));
        }
    }
}