using System;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Easynvest.Domain.Models;
using Easynvest.Domain.Configs;
using Easynvest.Services.IServices;
using Easynvest.Integration.Models;
using Easynvest.Integration.IServices;
using Newtonsoft.Json;
using System.Globalization;

namespace Easynvest.Services.Services
{
    public class FundsService : IFundsService
    {
        private readonly Parameters parameters;
        private readonly ILogger<IFundsService> logger;
        private readonly IRestClientService restClientService;

        public FundsService(ILogger<IFundsService> logger, IRestClientService restClientService, IOptionsSnapshot<Parameters> parameters)
        {
            this.logger = logger;
            this.parameters = parameters.Value;
            this.restClientService = restClientService;
        }

        public async Task<IList<Investiment>> GetInvestimentsAsync()
        {
            RestResponseModel<FundsResponseModel> fundsResponse = await restClientService.GetAsync<FundsResponseModel>(parameters.Endpoints.FundsApi, MediaTypeNames.Application.Json);
            if (fundsResponse.Error)
            {
                if (!string.IsNullOrEmpty(fundsResponse.ResponseText))
                    logger.LogError($"{nameof(GetInvestimentsAsync)} - {fundsResponse.ResponseText}");
                return new List<Investiment>();
            }

            if (fundsResponse.Data != null && fundsResponse.Data.Fundos != null && fundsResponse.Data.Fundos.Any())
            {
                IList<Investiment> investiments = fundsResponse.Data.Fundos
                        .Select(fund => new Investiment
                        {
                            Expiration = fund.DataResgate,
                            TotalValue = fund.ValorAtual,
                            InvestedValue = fund.CapitalInvestido,
                            Profitability = CalculateProfitability(fund),
                            Name = fund.Nome,
                            RedemptionValue = CalculateRedemptionValue(fund)
                        })
                        .ToList();

                logger.LogDebug($"investiments: {JsonConvert.SerializeObject(investiments)}");
                return investiments;
            }

            logger.LogInformation(parameters.Messages.EmptyInvestiment);
            return new List<Investiment>();
        }

        private double CalculateProfitability(FundModel fund)
        {
            double profitability = (fund.ValorAtual - fund.CapitalInvestido) * Convert.ToDouble(parameters.Profitability.FundsRate, CultureInfo.InvariantCulture);
            return Math.Round(profitability, int.Parse(parameters.Rounding));
        }

        private double CalculateRedemptionValue(FundModel fund)
        {
            TimeSpan totalTime = fund.DataResgate.Subtract(fund.DataCompra);
            TimeSpan runningTime = DateTime.Now.Subtract(fund.DataCompra);

            double percentageDifference = runningTime / totalTime;
            double redemptionValue = 0;
            if (percentageDifference > 0.5)
            {
                redemptionValue = fund.ValorAtual * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyTimeRate, CultureInfo.InvariantCulture));
            }
            else if (fund.DataCompra.AddMonths(int.Parse(parameters.Redemption.CustodyTimeMonth)) >= fund.DataResgate)
            {
                redemptionValue = fund.ValorAtual * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyTimeMonthRate, CultureInfo.InvariantCulture));
            }
            else
            {
                redemptionValue = fund.ValorAtual * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyDefaultRate, CultureInfo.InvariantCulture));
            }

            return Math.Round(redemptionValue, int.Parse(parameters.Rounding));
        }
    }
}