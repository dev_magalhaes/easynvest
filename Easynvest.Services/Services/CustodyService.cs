using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Easynvest.Domain.Models;
using Easynvest.Domain.Configs;
using Easynvest.Services.IServices;

namespace Easynvest.Services.Services
{
    public class CustodyService : ICustodyService
    {
        private readonly Parameters parameters;
        private readonly IFundsService fundsService;
        private readonly ILogger<CustodyService> logger;
        private readonly IFixedIncomeService fixedIncomeService;
        private readonly IDirectTreaseureService directTreasureService;

        public CustodyService(ILogger<CustodyService> logger, IDirectTreaseureService directTreasureService, IFixedIncomeService fixedIncomeService, IFundsService fundsService, IOptionsSnapshot<Parameters> parameters)
        {
            this.logger = logger;
            this.directTreasureService = directTreasureService;
            this.fixedIncomeService = fixedIncomeService;
            this.fundsService = fundsService;
            this.parameters = parameters.Value;
        }

        public async Task<ClientCustody> GetClientCustodyAsync()
        {
            IList<Investiment>[] responses = await Task.WhenAll(
                directTreasureService.GetInvestimentsAsync(),
                fixedIncomeService.GetInvestimentsAsync(),
                fundsService.GetInvestimentsAsync()
            );

            if (responses != null && responses.Any())
            {
                IList<Investiment> investiments = responses.SelectMany(x => x).ToList();

                return new ClientCustody
                {
                    Investiments = investiments,
                    TotalValue = investiments.Sum(x => x.InvestedValue)
                };
            }

            logger.LogInformation($"{nameof(GetClientCustodyAsync)} - {parameters.Messages.EmptyInvestiment}");
            return new ClientCustody
            {
                TotalValue = 0,
                Investiments = new List<Investiment> { }
            };
        }
    }
}