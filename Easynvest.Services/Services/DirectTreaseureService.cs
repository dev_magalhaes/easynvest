using System;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Easynvest.Domain.Models;
using Easynvest.Domain.Configs;
using Easynvest.Services.IServices;
using Easynvest.Integration.Models;
using Easynvest.Integration.IServices;
using Newtonsoft.Json;
using System.Globalization;

namespace Easynvest.Services.Services
{
    public class DirectTreaseureService : IDirectTreaseureService
    {
        private readonly Parameters parameters;
        private readonly IRestClientService restClientService;
        private readonly ILogger<IDirectTreaseureService> logger;

        public DirectTreaseureService(ILogger<IDirectTreaseureService> logger, IRestClientService restClientService, IOptionsSnapshot<Parameters> parameters)
        {
            this.logger = logger;
            this.parameters = parameters.Value;
            this.restClientService = restClientService;
            Console.WriteLine(this.parameters.Profitability.DirectTreasureRate);
        }

        public async Task<IList<Investiment>> GetInvestimentsAsync()
        {
            RestResponseModel<TDResponseModel> tdResponse = await restClientService.GetAsync<TDResponseModel>(parameters.Endpoints.DirectTreasureApi, MediaTypeNames.Application.Json);
            if (tdResponse.Error)
            {
                if (!string.IsNullOrEmpty(tdResponse.ResponseText))
                    logger.LogError($"{nameof(GetInvestimentsAsync)} - {tdResponse.ResponseText}");
                return new List<Investiment>();
            }

            if (tdResponse.Data != null && tdResponse.Data.Tds != null && tdResponse.Data.Tds.Any())
            {
                IList<Investiment> investiments = tdResponse.Data.Tds
                        .Select(td => new Investiment
                        {
                            Expiration = td.Vencimento,
                            TotalValue = td.ValorTotal,
                            InvestedValue = td.ValorInvestido,
                            Profitability = CalculateProfitability(td),
                            Name = td.Nome,
                            RedemptionValue = CalculateRedemptionValue(td)
                        })
                        .ToList();

                logger.LogDebug($"investiments: {JsonConvert.SerializeObject(investiments)}");
                return investiments;
            }
            
            logger.LogInformation(parameters.Messages.EmptyInvestiment);
            return new List<Investiment>();
        }

        private double CalculateProfitability(TDModel td)
        {
            double profitability = (td.ValorTotal - td.ValorInvestido) * Convert.ToDouble(parameters.Profitability.DirectTreasureRate, CultureInfo.InvariantCulture);
            return Math.Round(profitability, int.Parse(parameters.Rounding));
        }

        private double CalculateRedemptionValue(TDModel td)
        {
            TimeSpan totalTime = td.Vencimento.Subtract(td.DataDeCompra);
            TimeSpan runningTime = DateTime.Now.Subtract(td.DataDeCompra);

            double percentageDifference = runningTime / totalTime;
            double redemptionValue = 0;

            logger.LogDebug($"parameters.Redemption.CustodyTimeRate {parameters.Redemption.CustodyTimeRate}");
            logger.LogDebug($"Convert.ToDouble(parameters.Redemption.CustodyTimeRate): {Convert.ToDouble(parameters.Redemption.CustodyTimeRate, CultureInfo.InvariantCulture)}");
            if (percentageDifference > 0.5)
            {
                redemptionValue = td.ValorTotal * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyTimeRate, CultureInfo.InvariantCulture));
            }
            else if (td.DataDeCompra.AddMonths(int.Parse(parameters.Redemption.CustodyTimeMonth)) >= td.Vencimento)
            {
                redemptionValue = td.ValorTotal * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyTimeMonthRate, CultureInfo.InvariantCulture));
            }
            else
            {
                redemptionValue = td.ValorTotal * (1.0 - Convert.ToDouble(parameters.Redemption.CustodyDefaultRate, CultureInfo.InvariantCulture));
            }

            logger.LogDebug($"td.Nome: {td.Nome} - td.ValorTotal: {td.ValorTotal} - td.ValorInvestido: {td.ValorInvestido} - percentageDifference: {percentageDifference} - redemptionValue: {redemptionValue}");
            return Math.Round(redemptionValue, int.Parse(parameters.Rounding));
        }
    }
}