using System.Threading.Tasks;
using Easynvest.Domain.Models;

namespace Easynvest.Services.IServices
{
    public interface ICustodyService
    {
        Task<ClientCustody> GetClientCustodyAsync();
    }
}