using System.Collections.Generic;
using System.Threading.Tasks;
using Easynvest.Domain.Models;

namespace Easynvest.Services.IServices
{
    public interface IInvestimentService
    {
        Task<IList<Investiment>> GetInvestimentsAsync();
    }
}