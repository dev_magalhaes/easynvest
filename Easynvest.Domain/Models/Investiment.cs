using System;
using System.Runtime.Serialization;

namespace Easynvest.Domain.Models
{
    [DataContract]
    public class Investiment
    {
        [DataMember(Name = "nome")]
        public string Name { get; set; }

        [DataMember(Name = "valorInvestido")]
        public double InvestedValue { get; set; }

        [DataMember(Name = "valorTotal")]
        public double TotalValue { get; set; }

        [DataMember(Name = "vencimento")]
        public DateTime Expiration { get; set; }

        [DataMember(Name = "Ir")]
        public double Profitability { get; set; }

        [DataMember(Name = "valorResgate")]
        public double RedemptionValue { get; set; }
    }
}