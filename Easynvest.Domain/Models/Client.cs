using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Easynvest.Domain.Models
{
    [DataContract]
    public class ClientCustody
    {
        [DataMember(Name = "valorTotal")]
        public double TotalValue { get; set; }

        [DataMember(Name = "investimentos")]
        public IList<Investiment> Investiments { get; set; }
    }
}