using System.ComponentModel;

namespace Easynvest.Domain.Configs
{
    public class Profitability
    {
        public string DirectTreasureRate { get; set; } = "0.0";
        public string FixedIncomeRate { get; set; } = "0.0";
        public string FundsRate { get; set; } = "0.0";
    }
}