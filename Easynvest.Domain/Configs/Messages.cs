namespace Easynvest.Domain.Configs
{
    public class Messages
    {
        public string EmptyInvestiment { get; set; } = "Client has no investments";
    }
}