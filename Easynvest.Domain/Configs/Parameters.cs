using System.ComponentModel;

namespace Easynvest.Domain.Configs
{
    public class Parameters
    {
        public string Rounding { get; set; } = "4";
        public Messages Messages { get; set; } = new Messages();
        public Endpoints Endpoints { get; set; } = new Endpoints();
        public Redemption Redemption { get; set; } = new Redemption();
        public Profitability Profitability { get; set; } = new Profitability();
    }
}