namespace Easynvest.Domain.Configs
{
    public class Endpoints
    {
        public string DirectTreasureApi { get; set; } = "";
        public string FixedIncomeApi { get; set; } = "";
        public string FundsApi { get; set; } = "";

    }
}