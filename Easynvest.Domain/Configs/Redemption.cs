using System.ComponentModel;

namespace Easynvest.Domain.Configs
{
    public class Redemption
    {
        public string CustodyTimeRate { get; set; } = "0.0";
        public string CustodyTimeMonth { get; set; } = "0";
        public string CustodyTimeMonthRate { get; set; } = "0.0";
        public string CustodyDefaultRate { get; set; } = "0.0";
    }
}