using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Easynvest.Services.Services;
using Easynvest.Services.IServices;
using Easynvest.Integration.Services;
using Easynvest.Integration.IServices;
using Easynvest.Domain.Configs;
using System;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;

namespace Easynvest.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCaching();
            services.AddHealthChecks();
            services.AddOptions();
            services.AddControllers().AddNewtonsoftJson();
            services.AddScoped<ICustodyService, CustodyService>();
            services.AddScoped<IDirectTreaseureService, DirectTreaseureService>();
            services.AddScoped<IFixedIncomeService, FixedIncomeService>();
            services.AddScoped<IFundsService, FundsService>();
            services.AddHttpClient<IRestClientService, RestClientService>();

            services.Configure<Parameters>(Configuration.GetSection(nameof(Parameters)));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();

            app.UseResponseCaching();
            app.Use(async (context, next) =>
            {
                TimeSpan timeCache = DateTime.Today.AddDays(1).Subtract(DateTime.Now);
                context.Response.GetTypedHeaders().CacheControl =
                    new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
                    {
                        Public = true,
                        MaxAge = timeCache
                    };

                await next();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    AllowCachingResponses = false
                });
            });
        }
    }
}
