﻿
using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Easynvest.Services.IServices;

namespace Easynvest.Api.Controllers
{
    [ApiController]
    public class CustodyController : ControllerBase
    {
        private readonly ILogger<CustodyController> logger;

        public CustodyController(ILogger<CustodyController> logger)
        {
            this.logger = logger;
        }

        [HttpGet("/investments")]
        public async Task<IActionResult> GetInvestments([FromServices] ICustodyService custodyService)
        {
            try
            {
                return new JsonResult(await custodyService.GetClientCustodyAsync());
            }
            catch (Exception ex)
            {
                logger.LogError(ex, nameof(GetInvestments));
                return StatusCode(HttpStatusCode.InternalServerError.GetHashCode());
            }
        }
    }
}
